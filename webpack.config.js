const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

const debug = process.env.NODE_ENV !== "production";

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});
module.exports = {
    entry: "./build/index.js",
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    query: {
                        presets: ["react", "es2015"],
                        plugins: ["react-html-attrs", "transform-class-properties", "transform-decorators-legacy"],
                    }
                },

            }
        ]
    },
    output: {
        path: __dirname + "/dist/",
        filename: "bundle.js"
    },
    plugins: debug ? [htmlPlugin] : [
        htmlPlugin,
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}),
    ],
};