import React from "react";
import {Provider} from "react-redux";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Store} from "redux";
import {IEmpty} from "./common/IEmpty";
import {IAppState} from "./core/store/AppState";
import {configureStore} from "./core/store/tools/configureStore";
import {Bulletins} from "./modules/Bulletins/Bulletins";
import {Users} from "./modules/Users/Users";

export class App extends React.Component {
    private store: Store<IAppState>;

    constructor(props: IEmpty) {
        super(props);
        this.store = configureStore();
    }

    render(): React.ReactNode {
        return (
            <Provider store={this.store}>
                <Router>
                    <div>
                        <Route path={""} component={Users}/>
                        <Route path={"users"} component={Users}/>
                        <Route path={"bulletins"} component={Bulletins}/>
                    </div>
                </Router>
            </Provider>
        );
    }

}