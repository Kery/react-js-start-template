declare module "redux-stack" {
    // tslint:disable:interface-name
    interface ReduxStack {
        reducers?: any;
        enhancers?: any[];
        composers?: any[];
    }

    export function buildStack(stack: ReduxStack[]): { reducers: any, enhancer: any } ;
}