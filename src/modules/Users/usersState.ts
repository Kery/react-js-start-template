export interface IUsersState {
    users: string[];
}

export const UsersInitialState: IUsersState = {
    users: [],
};
