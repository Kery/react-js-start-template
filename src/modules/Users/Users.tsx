import React from "react";
import {Link} from "react-router-dom";

export class Users extends React.Component {
    render(): JSX.Element {
        return (
            <div>
                <h1>USERS</h1>
                <button type="button" className="btn btn-primary">Primary</button>
                <Link to="/bulletins">bulletins</Link>
            </div>
        );
    }
}