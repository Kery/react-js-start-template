import {IEmpty} from "../../common/IEmpty";
import {actionCreator} from "../../core/store/tools/actionCreator";

export class UsersActions {
    static getUsers = actionCreator.async<IEmpty, IEmpty, Error>("Users/GET_USERS");
}
