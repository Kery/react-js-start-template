import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../core/store/tools/newState";
import {UsersActions} from "./usersActions";
import {IUsersState, UsersInitialState} from "./usersState";

function getUsersStartedHandler(state: IUsersState): IUsersState {
    return newState(state, {});
}

function getUsersDoneHandler(state: IUsersState): IUsersState {
    return newState(state, {});
}

function getUsersFailedHandler(state: IUsersState): IUsersState {
    return newState(state, {});
}

export const usersReducer = reducerWithInitialState(UsersInitialState)
    .case(UsersActions.getUsers.started, getUsersStartedHandler)
    .case(UsersActions.getUsers.done, getUsersDoneHandler)
    .case(UsersActions.getUsers.failed, getUsersFailedHandler)
    .build();
