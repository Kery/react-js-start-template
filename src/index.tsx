import React from "react";
import ReactDOM from "react-dom";
import {App} from "./App";
import {appSettingsProvider} from "./core/settings/appSettingsProvider";
import {Playground} from "./modules/Playground/Playground";

ReactDOM.render(appSettingsProvider.settings.devOptions.showAllComponentsOnStart ? <Playground/> : <App/>, document.getElementById("app"));