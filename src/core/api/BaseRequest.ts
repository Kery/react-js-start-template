import {UrlHelper} from "../../common/helpers/urlHelper";

export abstract class BaseRequest {
  static handleError = (error: any): Promise<any> => {
    alert("Не удается выполнить действие");

    return Promise.reject(error.message || error);
  };
  private emptyResponse: EmptyResponse;

  constructor(protected baseurl: string) {
    this.emptyResponse = new EmptyResponse();
  }

  protected async fetch(url: string, config: any): Promise<any> {
    try {
      const headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
      };

      const response = await fetch(this.createUrl(url), Object.assign({headers: headers}, config));

      if (response.status == 204) {
        return this.emptyResponse;
      } else if (!response.status || response.status < 200 || response.status >= 300) {

        const error = await response.json();
        throw error;
      }

      return response;
    } catch (error) {
      //use some external logger
    }
  }

  protected createUrl(relativeUrl: string): string {
    return UrlHelper.create(relativeUrl, this.baseurl);
  }

}

class EmptyResponse {
  public json(): any {
    return null;
  }
}
