import {BaseRequest} from "./BaseRequest";

export const DEFAULT_PAGE_SIZE = 10;

export interface IGetBulletinByDynamicFilterRequest {
    filter: IFilter;
    paged: IPaged;
    sortParams: IBulletinSortParams[];
}

export interface IFilter {
    selectParams: string;
    whereParams: string;
    sortParams: string;
}

export interface IPaged {
    page: number;
    pageSize: number;
}

export type BulletinFieldName = "name" | "createdUtc" | "id" | "content" | "rating";

export interface IBulletinSortParams {
    fieldName: BulletinFieldName;
    isDesc: boolean;

}

export interface IBulletinDto {
    id: string;
    createdUtc: string | Date;
    updatedUtc: string | Date | null;
    deletedUtc: string | Date | null;
    number: number;
    userId: string;
    content: string;
    rating: number;
}

export class BulletinsApiRequest extends BaseRequest {
    constructor(protected baseurl: string) {
        super(baseurl);
        this.GetByPage = this.GetByPage.bind(this);
        this.GetByDynamicFilter = this.GetByDynamicFilter.bind(this);
    }

    GetByPage(page: number, pageSize: number, config?: any): Promise<IBulletinDto[]> {
        return this.fetch(
            `/api/User/GetByPage/${page}/${pageSize}`,
            Object.assign({
                method: "GET"
            }, config))
            .then((response) => response.json())
            .catch(BaseRequest.handleError);
    }

    GetByDynamicFilter(dynamicFilter: IGetBulletinByDynamicFilterRequest, config?: any): Promise<IBulletinDto[]> {
        return this.fetch(
            `/api/Bulletin/GetByDynamicFilter`,
            Object.assign({
                method: "POST",
                body: JSON.stringify(dynamicFilter)
            }, config))
            .then((response) => response.json())
            .catch(BaseRequest.handleError);
    }
}
