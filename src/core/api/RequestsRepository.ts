import {appSettingsProvider} from "../settings/appSettingsProvider";
import {BulletinsApiRequest} from "./BulletinsApiRequest";
import {UsersApiRequest} from "./UsersApiRequest";

export class RequestsRepository {
    bulletinsApiRequest = new BulletinsApiRequest(this.baseurl);
    usersApiRequest = new UsersApiRequest(this.baseurl);

    constructor(private baseurl: string) {
    }
}

export const requestsRepository = new RequestsRepository(appSettingsProvider.settings.serverUrl);