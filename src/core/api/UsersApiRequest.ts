import {BaseRequest} from "./BaseRequest";

export const DEFAULT_PAGE_SIZE = 10;

export interface IUserDto {
  id: string;
  createdUtc: string | Date;
  name: string;
}

export interface IGetUsersByDynamicFilterRequest {
  filter: IFilter;
  paged: IPaged;
  sortParams: IUserSortParams[];
}

export interface IFilter {
  selectParams: string;
  whereParams: string;
  sortParams: string;
}

export interface IPaged {
  page: number;
  pageSize: number;
}

export type UserFieldName = "name" | "createdUtc" | "id";

export interface IUserSortParams {
  fieldName: UserFieldName;
  isDesc: boolean;

}

export class UsersApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super(baseurl);
    this.GetByPage = this.GetByPage.bind(this);
    this.GetByDynamicFilter = this.GetByDynamicFilter.bind(this);
    this.AddUser = this.AddUser.bind(this);
    this.UpdateUser = this.UpdateUser.bind(this);
    this.DeleteUser = this.DeleteUser.bind(this);
  }

  protected GetByPage(page: number, pageSize: number, config?: any): Promise<IUserDto[]> {
    return this.fetch(
      `/api/User/GetByPage/${page}/${pageSize}`,
      Object.assign({
        method: "GET"
      }, config))
      .then((response) => response.json())
      .catch(BaseRequest.handleError);
  }

  protected GetByDynamicFilter(dynamicFilter: IGetUsersByDynamicFilterRequest, config?: any): Promise<IUserDto[]> {
    return this.fetch(
      `/api/User/GetByDynamicFilter`,
      Object.assign({
        method: "POST",
        body: JSON.stringify(dynamicFilter)
      }, config))
      .then((response) => response.json())
      .catch(BaseRequest.handleError);
  }

  protected AddUser(name: string, config?: any): Promise<string> {
    return this.fetch(
      `/api/User/Add`,
      Object.assign({
        method: "POST",
        body: JSON.stringify({name})
      }, config))
      .then((response) => response.json())
      .catch(BaseRequest.handleError);
  }

  protected UpdateUser(id: string, name: string, config?: any): Promise<string> {
    return this.fetch(
      `/api/User/Update`,
      Object.assign({
        method: "POST",
        body: JSON.stringify({name, id})
      }, config))
      .then((response) => response.json())
      .catch(BaseRequest.handleError);
  }

  protected DeleteUser(id: string, config?: any): Promise<void> {
    return this.fetch(
      `/api/User/Delete`,
      Object.assign({
        method: "POST",
        body: JSON.stringify(id)
      }, config))
      .then((response) => response.json())
      .catch(BaseRequest.handleError);
  }
}
