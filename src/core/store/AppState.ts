import {IUsersState, UsersInitialState} from "../../modules/Users/usersState";

export interface IAppState {
    users: IUsersState;
}

export const AppInitialState: IAppState = {
    users: UsersInitialState
};
