import {createStore, Store} from "redux";
import {buildStack} from "redux-stack";
import {createMainReduce} from "../AppReducer";
import {runAllSagas} from "../AppSaga";
import {IAppState} from "../AppState";
import {reduxLoggerInit} from "../middleware/logger";
import {promiseInit} from "../middleware/promise";
import {sagaInit, sagaMiddleware} from "../middleware/saga";
import {thunkInit} from "../middleware/thunk";

export function configureStore(): Store<IAppState> {
    const {enhancer} = buildStack([
        thunkInit,
        promiseInit,
        sagaInit,
        reduxLoggerInit]);
    const reducers = createMainReduce();

    const store = createStore(reducers, enhancer);
    runAllSagas(sagaMiddleware);

    return store;
}