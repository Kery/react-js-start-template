import {applyMiddleware} from "redux";
import {createLogger, ReduxLoggerOptions} from "redux-logger";
import {ReduxStack} from "redux-stack";
import {appSettingsProvider} from "../../settings/appSettingsProvider";
import {__DEV__} from "../../settings/getsettings";

const options: ReduxLoggerOptions = {
    diff: true,
    collapsed: true,
    predicate: (getState: () => any, action: any): boolean => {
        return __DEV__
            && !appSettingsProvider.settings.devOptions.disableReduxLogger;
    },
};
const logger = createLogger(Object.assign(options, appSettingsProvider.settings.devOptions.reduxLogger));

export const reduxLoggerInit: ReduxStack = {
    enhancers: __DEV__ ? [applyMiddleware(logger)] : []
};
