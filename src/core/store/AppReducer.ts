import {combineReducers, Reducer} from "redux";
import {usersReducer} from "../../modules/Users/usersReducer";
import {IAppState} from "./AppState";

export type Reducers<T> = {
    [P in keyof T]: Reducer<T[P]>;
};

export function createMainReduce(): Reducer<IAppState> {
    const reducers: Reducers<IAppState> = {
        users: usersReducer,
    };

    return combineReducers(reducers);

}
