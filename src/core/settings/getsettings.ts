import {IAppSettings} from "./appSettings";
// @ts-ignore
export const __DEV__ = process.env.NODE_ENV === "development";

const getSettings = (): IAppSettings => {
    // @ts-ignore
    if (require.resolve) {
        // @ts-ignore
        delete require.cache[require.resolve("../../../resources/settings/settings.json")];
    }
    // @ts-ignore
    let settings: IAppSettings = require("../../../resources/settings/settings.json");

    let environmentSettings: IAppSettings;
    switch (settings.environment) {
        case "Development":
            // @ts-ignore
            environmentSettings = require("../../../resources/settings/settings.Preset.Development.json");
            break;
        case "Test":
            // @ts-ignore
            environmentSettings = require("../../../resources/settings/settings.Preset.Test.json");
            break;
        case "Staging":
            // @ts-ignore
            environmentSettings = require("../../../resources/settings/settings.Preset.Staging.json");
            break;
        case "Production":
            // @ts-ignore
            environmentSettings = require("../../../resources/settings/settings.Preset.Production.json");
            break;
        default:
            throw new Error(`Not found settings for environment '${settings.environment}'`);

    }
    settings = Object.assign(settings, environmentSettings);

    if (typeof __DEV__ != "undefined" && __DEV__) {
        // @ts-ignore
        const localSettings = require("../../../resources/settings/localSettings.json");
        settings = Object.assign(settings, localSettings);
    }

    return settings;
};

declare const module: any; // we need this for use this file in tools/uploadBugsnag.js
module.exports.getSettings = getSettings;
