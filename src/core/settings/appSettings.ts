import {ReduxLoggerOptions} from "redux-logger";

export interface IAppSettings {
    appName: string;
    environment: "Development" | "Test" | "Staging" | "Production";
    serverUrl: string;
    build: number;
    devOptions: IDevOptions;
}

interface IDevOptions {
    // purgeStateOnStart: boolean; // for persist
    showAllComponentsOnStart: boolean;
    disableReduxLogger: boolean;
    reduxLogger?: ReduxLoggerOptions;
}